from django.urls import path

from receipts.views import (
    ReceiptListView,
    CreateReceiptView,
    ExpenseCategoryListView,
    AccountListView,
    ExpenseCategoryCreate,
    AccountCreateView,
)


urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", CreateReceiptView.as_view(), name="create_receipt"),
    path(
        "categories/",
        ExpenseCategoryListView.as_view(),
        name="list_categories",
    ),
    path("accounts/", AccountListView.as_view(), name="account_list"),
    path(
        "categories/create/",
        ExpenseCategoryCreate.as_view(),
        name="create_category",
    ),
    path('accounts/create/', AccountCreateView.as_view(), name="create_account"),
]
